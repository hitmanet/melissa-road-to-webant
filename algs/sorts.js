const mergeSort = (arr) =>{
    if (arr.length === 1){
        return arr
    }

    const middle = Math.floor(arr.length/2)
    const left = arr.slice(0, middle)
    const right = arr.slice(middle)
    return merge(mergeSort(left), mergeSort(right))
}


const merge = (left, right) =>{
    const result = []
    let indexRight = 0
    let indexLeft = 0

    while (indexLeft < left.length && indexRight < right.length){
        if (left[indexLeft] < right[indexRight]){
            result.push(left[indexLeft])
            indexLeft++
        }
        else{
            result.push(right[indexRight])
            indexRight++
        }
    }
    return result.concat(left.slice(indexLeft)).concat(right.slice(indexRight))
}

console.log(mergeSort([2,6,1,6,2,72,721,2,34,1432,82,82]))


//////////////////////////////////////////////////////////

const swap = (items, firstIndex, secondIndex) =>{
    const temp = items[firstIndex]
    items[firstIndex] = items[secondIndex]
    items[secondIndex] = temp
}

const partition = (items, left, right) => {
    const pivot = items[Math.floor((right + left) / 2)]
    let i = left
    let j = right
    while (i <= j) {
        while (items[i] < pivot) {
            i++
        }
        while (items[j] > pivot) {
            j--
        }
        if (i <= j) {
            swap(items, i, j)
            i++
            j--
        }
    }
    return i
}


const quickSort = (items, left = 0, right = items.length-1) => {
    let index
    if (items.length > 1) {
        index = partition(items, left, right)
        if (left < index - 1) {
            quickSort(items, left, index - 1)
        }
        if (index < right) {
            quickSort(items, index, right)
        }
    }
    return items
}

